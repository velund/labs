package Laba6;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Ape extends Animal {
    String name;
    int intellect;
    int srength;

    public Ape(int weight, double length, String color, String sex, String habitat, String name, int srength) {
        super(weight, length, color, sex, habitat);
        this.name = name;
        this.srength = srength;
    }
    public Ape(int weight, double length, String color, String sex, String habitat, int srength) {
        super(weight, length, color, sex, habitat);
        this.srength = srength;
    }
    public Ape(int weight, double length, String color, String sex, String habitat, String name, int intellect, int srength) {
        super(weight, length, color, sex, habitat);
        this.name = name;
        this.intellect = intellect;
        this.srength = srength;
    }

    public void vojak(Ape ape){
        if (this.srength+this.weight+this.length > ape.srength+ape.weight+ape.length) System.out.println("Вожаком станет " + this);
        else System.out.println("Вожак станет "+ ape);
    }
}
