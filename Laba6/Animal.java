package Laba6;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Animal extends Fish {

    String habitat;
    int speed;

    public Animal(int weight, double length, String color, String sex, String habital, int speed) {
        super(weight, length, color, sex);
        this.habitat = habital;
        this.speed = speed;
    }

    public Animal(int weight, double length, String s, String color, String sex) {
        super(weight, length, color, sex);
    }

    public void hunter(Human human) {
        if ((this.speed + this.weight + this.length) / human.accuracy > 10)
            System.out.println("Животному не удасться убежать и " + human.name + " будет сыт");
        else System.out.println("Животное может убежать и " + human.name + " не будет сегодня кушать");
    }

    public void battelAnimal(Animal animal) {
        if (this.habitat == animal.habitat) {
            if (this.weight + this.length > animal.length + animal.weight)
                System.out.println("Животное " + this + " одержит победу");
            else System.out.println("Животное " + animal + " одержит победу");
        } else System.out.println("Животные не смогут встертиться так как у нах разный ареал");

    }

}

