package Laba6;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Pet {
    String sex;
    int weight;
    int age;

    public Pet(String sex, int weight, int age) {
        this.sex = sex;
        this.weight = weight;
        this.age = age;
    }
}
