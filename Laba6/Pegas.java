package Laba6;

/**
 * Created by Владислав on 26.03.2016.
 * Написать два класса: Horse(лошадь) - в конструкторе задается кличка лошади и Pegas(пегас)
 * - релизующий метод полет(сообщает какая лошадь летит). Унаследовать пегаса от лошади.
 */
public class Pegas extends Horse {

    public Pegas(String name) {
        super(name);
    }
    public void flyPegas(){
        System.out.println(this.name +" flies");
    }
}
