package Laba6;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Human extends Ape {

    boolean superability;
    int accuracy;

    public Human(int weight, double length, String color, String sex, String habitat, String name, int intellect, boolean superability) {
        super(weight, length, color, sex, habitat, name, intellect);
        this.superability=superability;
    }
    public Human(int weight, double length, String color, String sex, String habitat, String name, int intellect, boolean superability, int accuracyuracy) {
        super(weight, length, color, sex, habitat, name, intellect);
        this.superability=superability;
        this.accuracy=accuracy;
    }

    public void training(Ape ape) {
        if (this.intellect > ape.intellect) System.out.println(this.name + " может дрессировать " + ape.name);
        else System.out.println(ape.name + " может дрессировать " + this.name);
    }

    public void save(Human human){
        if ((this.superability) || (!human.superability)){
            System.out.println(this.name+" может спасти "+ human.name);
        }
        else if ((!this.superability) || (!human.superability)){
            System.out.println(this.name+" не может спасти "+ human.name);
        }
        else if ((this.superability) || (human.superability)){
            System.out.println(this.name+" может не спасать "+ human.name + ","+human.name+" самостоятельно может спастись");
        }
        else if ((!this.superability) || (human.superability)){
            System.out.println(this.name+" не может спасти "+ human.name+ ", зато "+ human.name + " может спасти " + this.name);
        }



    }

}
