package Laba6;

import javax.xml.bind.SchemaOutputResolver;

/**
 * Created by Владислав on 28.03.2016.
 * . Написать два класса: Horse(лошадь) - в конструкторе задается кличка лошади и Pegas(пегас) - релизующий метод полет(сообщает какая лошадь летит). Унаследовать пегаса от лошади.
 * 2. Написать три класса: Pet(домашнее животное) - с полями вес, пол, возраст, Cat(кот) с полем имя и Dog(собака) с полем имя.
 * Унаследовать кота и собаку от животного. В классах кот и собака реализовать методы вывода информации и о животных
 * 3. Написать четыре класса: Fish(Рыбы), Animal(Животные), Ape(Обезьяны), Human(Человек). Унаследовать животных от рыб,
 * обезьян от животных и человека от обезьян. Поля и методы разработать самостоятельно.
 * 4. Скрыть все внутренние переменные класса Cat, добавить методы работы с этими переменными
 */
public class Test {
    public static void main(String[] args) {
        Fish fish1 = new Fish(3, 1, "white", "M");
        Fish fish2 = new Fish(4, 2, "black", "W");
        Animal animal = new Animal(3, 1, "white", "M", "12");
        Ape ape1 = new Ape(13, 1.2, "orange", "M", "zoo", "Jon", 200 );
        Human human1 = new Human(82, 1.88, "black", "M", "zoo_halupa", "Petr", 180, false);
        Human human2 = new Human(80, 1.77, "black", "M", "zoo_halupa", "Fill", 100, true);
        Dog dog = new Dog("M", 30, 8, "Balto");
        Cat cat = new Cat("m",12,56,"Vaska", 2);


        dog.printDog1();
        human1.training(ape1);
        fish1.equals(fish2);

        human1.save(human2);
        human2.save(human1);

    }
}
