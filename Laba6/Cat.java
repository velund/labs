package Laba6;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Cat extends Pet {
    String name;
    private int  legentClutches;

    public Cat(String sex, int weight, int age, String name, int legentClutches) {
        super(sex, weight, age);
        this.name = name;
        this.legentClutches = legentClutches;
    }

    public void printCat() {
        System.out.println("Имя кота:" + this.name + " пол: " + this.sex + " возраст: " + this.age + " вес:" + this.weight);
    }

    public void trimCatClaws(int a){
        System.out.println("Когти кота подстрижены и равны: " + this.cut(a));
    }

    private int cut(int a){
        if (this.legentClutches > a)
        return this.legentClutches - a;
        else return 0;
    }


}
