package Laba6;

/**
 * Created by Владислав on 28.03.2016.
 */
public class Dog extends Pet {
    String name;
    public Dog(String sex, int weight, int age, String name) {
        super(sex, weight, age);
        this.name=name;
    }
    public void printDog1(){
        printDog();
    }
    private void printDog() {
        System.out.println("Имя собаки: " + this.name + " пол: " + this.sex + " возраст: " + this.age + " вес: " + this.weight);
    }
}
