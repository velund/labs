package laba2;

/**
 * Created by Владислав on 01.02.2016.
 * Рисуем треугольник
 * Используя цикл for вывести на экран прямоугольный треугольник из восьмёрок со сторонами
 */
class zad3 {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {             //Прямоугольный треугольник 10x10
            for (int j = 0; j < 10; j++) {
                System.out.print("8");
                if (i == j) break;
            }
            System.out.println("");
        }

    }
}
