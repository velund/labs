package laba2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 01.02.2016.
 * Координатные четверти Ввести с клавиатуры два целых числа, которые будут координатами точки,
 * не лежащей на координатных осях OX и OY.
 * Вывести на экран номер координатной четверти, в которой находится данная точка.
 */
class zad7 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        int a;                          //координата x
        int b;                          //координата y
        //Исключения
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
        } catch (Exception e) {
            System.out.println("Ошибка ввода");
            return;
        }

        if ((a > 0) && (b > 0)) System.out.println("1");      //Определене четверти координат
        if ((a < 0) && (b > 0)) System.out.println("3");      //
        if ((a < 0) && (b < 0)) System.out.println("4");      //
        if ((a > 0) && (b < 0)) System.out.println("2");      //
    }
}
