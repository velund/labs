package laba2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 01.02.2016.
 * Рисуем прямоугольник Ввести с клавиатуры два числа m и n.
 * Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
 */
class zad2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        int a, b;

        //Исключения
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
            if ((a < 0) || ((a == 0)) || ((b == 0) || (b < 0))) {
                System.out.println("Ошибка ввода");
                return;
            }
        } catch (Exception e) {
            System.out.println("Ошибка ввода");
            return;
        }

        for (int i = 0; i < a; i++) {              //Прямоугольник а-длинны и b-ширины
            for (int j = 0; j < b; j++) {
                System.out.print("8");
            }
            System.out.println("");
        }


    }
}
