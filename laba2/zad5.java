package laba2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 01.02.2016.
 * Сравнить имена Ввести с клавиатуры два имени, и если имена одинаковые, вывести сообщение «Имена идентичны».
 * Если имена разные, но их длины равны – вывести сообщение – «Длины имен равны».
 */
class zad5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name1 = reader.readLine();
        String name2 = reader.readLine();

        for (int i = 0; i < 10; i++) {
            if ((name1.contains("" + i)) || (name2.contains("" + i))) {
                System.out.println("Ошибка");
                return;
            }
        }

        if (name1.equals(name2)) System.out.println("имена индентичны");                    //Сравнение имён
        else if (name1.length() == name2.length()) System.out.println("длины имен равны");

    }
}
