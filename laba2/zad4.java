package laba2;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 01.02.2016.
 * Минимум двух чисел. Ввести с клавиатуры два числа, и вывести на экран минимальное из них
 */
class zad4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        int a;
        int b;
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
        } catch (Exception e) {
            System.out.println("Ошибка ввода");
            return;
        }

        if (a > b) System.out.println(a);         //Вывод наибольшего
        else System.out.println(b);
    }
}
