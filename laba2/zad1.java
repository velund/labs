package laba2;

/**
 * Created by Владислав on 01.02.2016.
 * Четные числа Используя цикл for вывести на экран чётные числа от 1 до 100 включительно.
 * Через пробел либо с новой строки.
 */
public class zad1 {
    public static void main(String[] args) {            //Вывод четных чисел
        for (int i = 1; i < 101; i++) {
            if (i % 2 == 0) System.out.println(i);
        }
    }

}
