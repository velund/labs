package laba2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 01.02.2016.
 * Минимум четырех чисел Написать функцию, которая вычисляет минимум из четырёх чисел.
 * Функция min(a,b,c,d) должна использовать (вызывать) функцию min(a,b).
 */
class zad6 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        String sc = reader.readLine();
        String sd = reader.readLine();
        int a,b,c,d;
        //Исключения
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
            c = Integer.parseInt(sc);
            d = Integer.parseInt(sd);
        } catch (Exception e) {
            System.out.println("Ошибка ввода");
            return;
        }

        System.out.println(min2(min2(a, b), min2(c, d)));
        min2(min2(a, b), min2(c, d));              //нахождение минимального из 4 чисел
    }


    public static int min2(int a, int b) {           //нахождение минимального
        if (a < b) return a;
        else return b;
    }


}
