package Laba72;

/**
 * Created by Владислав on 01.06.2016.
 * 1.	Сделать класс Pet (лабораторная работа №6) абстрактным.
 * 2.	Создать абстрактный класс Pet с полями name, age, hungry(хочет есть) и
 *   абстрактный метод voice(голос). Создать классы Snake, Dog, PatrolDog, Cat, Fish и наследники класса Pet.
 *  В каждом классе реализовать метод voice.

 */
public abstract class Pet {
    int age;
    String name;
    boolean hungry;

    public abstract void voice();
}


class Snake extends Pet{
    public void voice(){
        System.out.println("Тшшшшшш");
    }
}

class Dog extends Pet{
    public void voice(){
        System.out.println("Гав");
    }
}

class Fish extends Pet{
    public void voice(){
        System.out.println("бульк бульк бульк");
    }
}

class PatrolDog extends Pet{
    public void voice(){
        System.out.println("Тревога!!!");
    }
}

class Cat extends Pet{
    public void voice(){
        System.out.println("Мяууу");
    }
}
