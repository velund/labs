package Laba72;

/**
 * Created by Владислав on 01.06.2016.
 * Реализовать два интерфейса PassangersAuto(описать метод перевозки пассажиров)
 * и CargoAuto (описать метод перевозки груза). Написать классы Truck, Sedan, Pickup реализующие эти интерфейсы
 */
interface PassangersAuto {

    public void vosKolPas(int j);

}

interface CargoAuto {

    public void vosGrus(int j);
}

class Sedan implements PassangersAuto {

    public void vosKolPas(int kolpas) {
        System.out.println("Автобус вмещает максимум 50 пассажиров");
        if (kolpas <= 10) {
            System.out.println("Скорость равно: 120");
        } else if (kolpas <= 20) {
            System.out.println("Скорость равно: 110");
        } else if (kolpas <= 30) {
            System.out.println("Скорость равно: 100");
        } else if (kolpas <= 40) {
            System.out.println("Скорость равно: 95");
        } else if (kolpas <= 50) {
            System.out.println("Скорость равно: 90");
        } else if (kolpas > 50) {
            System.out.println("Выгнять лишних пассажиров");
        }

    }
}

class Truc implements CargoAuto {
    int skor = 0;

    @Override
    public void vosGrus(int kolgrus) {
        System.out.println("Грузоподъемность грузовика 5 тонн");
        if (kolgrus <= 1000) {
            System.out.println("Скорость равно: 100");
        } else if (kolgrus <= 2000) {
            System.out.println("Скорость равно: 95");
        } else if (kolgrus <= 3000) {
            System.out.println("Скорость равно: 90");
        } else if (kolgrus <= 4000) {
            System.out.println("Скорость равно: 85");
        } else if (kolgrus <= 5000) {
            System.out.println("Скорость равно: 80");
        } else if (kolgrus > 5000) {
            System.out.println("Не поедет");
        }

    }
}

class Pickup implements CargoAuto, PassangersAuto {

    @Override
    public void vosGrus(int kolpas) {
        System.out.println("Пикап вмещает максимум 1 тонну");
        if (kolpas <= 200) {
            System.out.println("Скорость равно: 100");
        } else if (kolpas <= 400) {
            System.out.println("Скорость равно: 95");
        } else if (kolpas <= 600) {
            System.out.println("Скорость равно: 90");
        } else if (kolpas <= 800) {
            System.out.println("Скорость равно: 85");
        } else if (kolpas <= 1000) {
            System.out.println("Скорость равно: 80");
        } else if (kolpas > 1000) {
            System.out.println("Не поедет");
        }
    }

    @Override
    public void vosKolPas(int kolgrus) {
        System.out.println("Пикап вмещает максимум 4 пассажиров");
        if (kolgrus == 1) {
            System.out.println("Скорость равно: 100");
        } else if (kolgrus == 2) {
            System.out.println("Скорость равно: 98");
        } else if (kolgrus == 3) {
            System.out.println("Скорость равно: 96");
        } else if (kolgrus > 3) {
            System.out.println("Они не влезут");
        }
    }
}
