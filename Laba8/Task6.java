package Laba8;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Владислав on 14.05.2016.
 */
public class Task6 {
    public static void main(String[] args) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("1", "lampa");
        map.put("2", "body");
        map.put("3", "face");
        map.put("4", "dream");
        map.put("5", "Timon");
        map.put("6", "Pumba");
        map.put("7", "Simba");
        map.put("8", "Shram");
        map.put("9", "Mufasa");
        map.put("10", "Makaka");

        for (Map.Entry<String, Object> print :
                map.entrySet()) {
            System.out.println(print.getKey() + "-" + print.getValue());
        }
    }
}
