package Laba8;

import java.util.HashMap;
import java.util.Map;

/**
 * 4.	Вывести на экран список ключей Есть коллекция HashMap<String, String>, туда занесли 10 различных строк.
 * Вывести на экран список ключей, каждый элемент с новой строки.
 * Created by Владислав on 18.04.2016.
 */
public class Task4 {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "sdf");
        map.put("2", "sdf");
        map.put("3", "sdf");
        map.put("4", "sdf");
        map.put("5", "sdf");
        map.put("6", "sdf");
        map.put("7", "sdf");
        map.put("8", "sdf");
        map.put("9", "sdf");
        map.put("10", "sdf");

        for (Map.Entry<String, String> pair : map.entrySet()) {
            System.out.println(pair.getKey());
        }
    }
}
