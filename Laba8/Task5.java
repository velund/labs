package Laba8;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Владислав on 14.05.2016.
 */
public class Task5 {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "q");
        map.put("2", "qw");
        map.put("3", "qwe");
        map.put("4", "qwer");
        map.put("5", "qwert");
        map.put("6", "qwerty");
        map.put("7", "qwertyu");
        map.put("8", "qwertyui");
        map.put("9", "qwertyuio");
        map.put("10", "qwertyuiop");

        for (Map.Entry<String, String> pair : map.entrySet()
                ) {
            System.out.println(pair.getValue());
        }
    }
}
