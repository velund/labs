package Laba8;

import java.util.HashMap;
import java.util.Map;

/**
 * 3.	Коллекция HashMap из котов Есть класс Cat, с полем имя (name, String).Создать коллекцию HashMap<String, Cat>.
 * Добавить в коллекцию 10 котов, в качестве ключа использовать имя кота.
 * Вывести результат на экран, каждый элемент с новой строки.
 * Created by Владислав on 18.04.2016.
 */
public class Task3 {
    public static void main(String[] args) {
        HashMap<String, Cat> map = new HashMap<>();
        Cat cat1 = new Cat("Vasay");
        Cat cat2 = new Cat("Petay");
        Cat cat3 = new Cat("Barsik");
        Cat cat4 = new Cat("Snejok");
        Cat cat5 = new Cat("Diablo");
        Cat cat6 = new Cat("Grom");
        Cat cat7 = new Cat("Mest'");
        Cat cat8 = new Cat("Voin");
        Cat cat9 = new Cat("Gnom");
        Cat cat10 = new Cat("Ork");
        map.put(cat2.name, cat1);
        map.put(cat3.name, cat2);
        map.put(cat4.name, cat3);
        map.put(cat5.name, cat4);
        map.put(cat6.name, cat5);
        map.put(cat7.name, cat6);
        map.put(cat8.name, cat7);
        map.put(cat9.name, cat8);
        map.put(cat1.name, cat9);
        map.put(cat10.name, cat10);

        for (Map.Entry<String, Cat> pair :
                map.entrySet()) {
            System.out.println(pair.getKey());
        }
    }





    public static class Cat {
        String name;

        public Cat(String name) {
            this.name = name;
        }

        public String getName(String name) {
            return this.name = name;
        }
    }
}
