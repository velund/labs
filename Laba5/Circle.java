package Laba5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Владислав on 10.03.2016.
 * 1. Создайте в классе Circle метод, вычисляющий длину окружности.
 * 2.Создайте в классе Circle метод, перемещающий центр круга в случайную точку квадрата координатной плоскости с
 * диагональю от [-99;-99] до [99;99]. Обратите внимание на то, что требуется создать обычный метод, применимый к уже
 * существующему объекту, а не конструктор создающий новый объект.
 * 3.Измените в классе Circle конструктор по умолчанию так, чтобы в момент создания объекта с его помощью, координаты
 * центра и радиус окружности пользователь вводил с клавиатуры.
 * 4.Создайте в классе Circle метод, вычисляющий расстояние между центрами двух окружностей.
 * 5.Создайте в классе Circle метод, проверяющий, касаются ли окружности в одной точке. Учтите, что возможен вариант,
 * когда одна окружность содержится внутри другой и при этом всё равно возможно касание в одной точке.
 */
public class Circle {
    public double r;
    public double x;
    public double y;

    public static void main(String[] args)  {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle();
        System.out.println(centerDistance(circle1, circle2));
        System.out.println(circle1.circumference());
        if (contactPoint(circle1,circle2)){
        }


        circle1.centerCircleRandom();
        System.out.println(circle1.x + " " + circle1.y);
    }

    //Нахождение длины окружности
    public double circumference() {
        return 2 * Math.PI * r;
    }

    //Изменение центра окружности (рандом от -99 до 99)
    public void centerCircleRandom() {
        this.x = (int) (Math.random() * 199) - 99;
        this.y = (int) (Math.random() * 199) - 99;

    }

    //Коструктор. Передача параметров объекту класса Circle
    public Circle()  {

        double a = 0, b = 0, c = 0;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                a = Double.parseDouble(reader.readLine());
                b = Double.parseDouble(reader.readLine());
                c = Double.parseDouble(reader.readLine());
                if (c <= 0) System.out.println("Радиус окружности не может быть равен нулю или быть отрицательным");
                    else break;
            }
        } catch (Exception e) {
            System.out.println("не правильный ввод данных");
        }
        this.x = a;
        this.y = b;
        this.r = c;
    }

    //Нахождение расстояния между двумя точками
    public static double centerDistance(Circle o1, Circle o2) {
        return (Math.sqrt((o2.x * o2.x - o1.x * o1.x) + (o2.y * o2.y - o1.y * o1.y)));
    }

    //Проверение касаються  ли окружности в одной точке
    public static boolean contactPoint(Circle o1, Circle o2) {
        if ((centerDistance(o1, o2) == (o1.r + o2.r)) || (centerDistance(o1, o2) == Math.abs(o2.r - o1.r))) return true;
        else return false;
    }

}
