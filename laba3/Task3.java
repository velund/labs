/**
 * Created by Владислав on 15.02.2016.
 * Создайте массив из 15 случайных целых чисел из отрезка [0;9].
 * Выведите массив на экран. Подсчитайте сколько в массиве чётных
 * элементов и выведете это количество на экран на отдельной строке.
 */
package laba3;

class Task3 {
    public static void main(String[] args) throws Exception {
        //Инициализируем массив
        int[] mas = new int[15];
        int n=0;
        for (int i = 0; i < 15; i++) {
            //Присваевываем рандомные значения ячейкам массива в диапазоне [0;9]
            mas[i]=(int)(Math.random()*10);
            if (mas[i]%2==0)n++;
            System.out.print(mas[i]+" ");
        }
        System.out.println();
        System.out.println("Количество четных чисел: "+ n);
    }
}
