/**
 * Created by Владислав on 14.02.2016.
 * 2.	Создайте массив из всех нечётных
 * чисел от 1 до 99, выведите его на экран в строку,
 * а затем этот же массив выведите на экран тоже в строку, но в обратном порядке
 */
package laba3;


class Task2 {
    public static void main(String[] args)throws Exception{
        int k=1,n=50;
        int[] mas = new int[n];
        for (int i = 0; i < n; i++) {
            mas[i]=k;
            k= k+2;
            System.out.print(mas[i]+" ");
        }
        System.out.println();
        for (int j = n-1; j >= 0; j--) {
            System.out.print(mas[j]+" ");
        }
    }
}
