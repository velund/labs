/**
 * Created by Владислав on 15.02.2016.
 * Создать двумерный массив из 8 строк по 5 столбцов
 * в каждой из случайных целых чисел из отрезка [10;99]. Вывести массив на экран.
 */
package laba3;

class Task4 {
    public static void main(String[] args)throws Exception {
        int[][] mas = new int[8][5];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
                mas[i][j] = (int) (Math.random() * 90) + 10;
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }
}
