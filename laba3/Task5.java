/**
 * Created by Владислав on 15.02.2016.
 * Создать двумерный массив из 7 строк по 4 столбца в каждой
 * из случайных целых чисел из отрезка [-5;5]. Вывести массив на экран.
 * Определить и вывести на экран индекс строки с наибольшим по модулю
 * произведением элементов. Если таких строк несколько, то вывести индекс
 * первой встретившейся из них.
 */
package laba3;

class Task5 {
    public static void main(String[] args) {
        //Инициализация массива
        int[][] mas = new int[7][4];
        int proizv_stroki = 1, proiz_naibolshee = 0, p1 = 0, namber_str = 0, nomer = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                //Рандом в диапазоне от -5 до 5
                mas[i][j] = (int) (Math.random() * 11 - 5);
                System.out.print(mas[i][j] + " ");
                //нахождение произведения строки по модулю
                proizv_stroki *= mas[i][j];
                p1 = Math.abs(proizv_stroki);
            }
            namber_str++;
            System.out.println();
            //нахождение наибольшего призведения чисел из строк
            if (p1 > proiz_naibolshee) {
                proiz_naibolshee = p1;
                nomer = namber_str;
            }
            proizv_stroki = 1;
        }
        System.out.println("Номер индекса строки " + nomer + ". Произведение равно: " + proiz_naibolshee);
    }
}
