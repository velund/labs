/**
 * Created by Владислав on 14.02.2016.
 * Создайте массив из всех чётных чисел от 2 до 20 и выведите элементы массива
 * на экран сначала в строку, отделяя один элемент от другого пробелом, а затем в столбик
 */

package laba3;


class task1 {
    public static void main(String[] args) {
        //Объевляем массив и инициализируем его
        int[] mass = new int[10];
        int j = 0;
        for (int i = 0; i < 10; i++) {
            //Заполняем массив чётными числами
            j = j + 2;
            mass[i] = j;
            //Вывод массива в строку
            System.out.print(mass[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            //Вывод массива в столбец
            System.out.println(mass[i]);

        }
    }
}
