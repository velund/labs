package Laba7;

import Laba7.Task2.Bird;
import Laba7.Task2.Cat;
import Laba7.Task2.Dog;
import Laba7.Task2.Lamp;

/**
 * Created by Владислав on 04.04.2016.
 * 1.	Переопределить метод getName в классе Whale(Кит), чтобы программа выдавала: Я не корова, Я – кит.
 * 2.	Написать метод, который определяет, объект какого класса ему передали, и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
 * 3.	Создать два класса Cat(кот) и Dog(собака), класс Dog(собака) происходит от Cat(кот). Определить метод getChild в классах Cat(кот)
 * и Dog(собака), чтобы кот порождал кота, а собака – собаку.
 * <p>
 * <p>
 * <p>
 * <p>
 * 4.	Написать public static методы: int min(int, int), long min(long, long), double min(double, double). Каждый метод должен возвращать
 * минимальное из двух переданных в него чисел.
 * 5.	Написать public static методы: int max(int, int), long max (long, long), double max (double, double).
 * Каждый метод должен возвращать максимальное из двух переданных в него чисел.
 */
public class Test {
    public static void main(String[] args) {
        Cow cow = new Whale();
        Whale whale = new Whale();
//        System.out.println(whale.getName());
//        System.out.println(cow.getName());
        cow.getName();

//        Bird bird = new Bird();
//        Cat cat = new Cat();
//        Dog dog = new Dog();
//        Lamp lamp = new Lamp();
//
//        opredelitel(cat);
        opredelitel(whale);


    }

    //Метод определения класса
    public static void opredelitel(Object obj) {

        if (obj instanceof Whale) {
            Whale w =(Whale) obj;
            System.out.println("я кит");
        }
        if (obj instanceof Cow) {

        }
        if (obj instanceof Bird) System.out.println("Птица");
        if (obj instanceof Cat) System.out.println("Кошка");
        if (obj instanceof Dog) System.out.println("Собака");
        if (obj instanceof Lamp) System.out.println("Лампа");

    }

    public void printClass(Cat cat) {
        cat.getName();
    }

    //2,3 Задание
    public void print(int a) {
        System.out.println("квадрат числа" + a + "=" + a * a);
    }

    public void print(Integer a) {
        System.out.println("объект класса Integer является ссылкой на " + a);
    }


    public void print(String s) {
        System.out.println(s);
    }

    public void print(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }

    public void print(String s, int a) {
        System.out.println(s + a);
    }

    public void print(int n, String s) {
        System.out.println(n + s);
    }

    //4,5 ЗАДАНИЕ
    public static int min(int a, int b) {
        if (a < b) return a;
        else return b;
    }

    public static long min(long a, long b) {
        if (a < b) return a;
        else return b;
    }

    public static double min(double a, double b) {
        if (a < b) return a;
        else return b;
    }

    public static int max(int a, int b) {
        if (a > b) return a;
        else return b;
    }

    public static long max(long a, long b) {
        if (a > b) return a;
        else return b;
    }

    public static double max(double a, double b) {
        if (a > b) return a;
        else return b;
    }


}
