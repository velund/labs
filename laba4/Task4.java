package laba4;

/**
 * Created by Владислав on 27.02.2016.
 * Выясните экспериментальном путём, начиная с какого элемента
 * последовательности Фибоначчи, вычисление с использованием рекурсии
 * становится неприемлемым (занимает более минуты по времени).
 */
public class Task4 {
    public static void main(String[] args) {
        int n = 0;
        long startTime, finisTime;
        for (int i = 1; i < 100; i++) {
            startTime=System.currentTimeMillis();
            sequenceFibonachi(i);
            finisTime = System.currentTimeMillis();
            if ((finisTime-startTime) > 60000) {
               System.out.println("c " + i + " числа использование рекурсии становиться неприемлимым");
                return;
            }
        }

    }
    //нахождение числа Фибоначи
    public static int sequenceFibonachi(int n) {
        if (n == 1) return 1;
        if (n == 2) return 1;
        return ((sequenceFibonachi(n - 1) + sequenceFibonachi(n - 2)));
    }
}
