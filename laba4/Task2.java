

/**
 * Created by Владислав on 22.02.2016.
 * Создать метод, который будет выводить указанный массив на экран в строку.
 * С помощью созданного метода и метода из предыдущей задачи заполнить 5 массивов
 * из 10 элементов каждый случайными числами и вывести все 5 массивов на экран,
 * каждый на отдельной строке.
 */
package laba4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Task2 {
    // Создание 5 массивов
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        int a, b;
        //Исключения
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
            if (a >= b) {
                System.out.println("Ошибка: задан не верный диапазон");
                return;
            }
        } catch (Exception e) {
            System.out.println("Ошибка: не правильный ввод данных");
            return;
        }
        int[] array1 = new int[10];
        int[] array2 = new int[10];
        int[] array3 = new int[10];
        int[] array4 = new int[10];
        int[] array5 = new int[10];
        //Заполнение массивов
        for (int i = 0; i < 10; i++) {
            array1[i] = Task1.random_AB(a, b);
        }
        for (int i = 0; i < 10; i++) {
            array2[i] = Task1.random_AB(a, b);
        }
        for (int i = 0; i < 10; i++) {
            array3[i] = Task1.random_AB(a, b);
        }
        for (int i = 0; i < 10; i++) {
            array4[i] = Task1.random_AB(a, b);
        }
        for (int i = 0; i < 10; i++) {
            array5[i] = Task1.random_AB(a, b);
        }
        Task1.random_AB(a, b);

        masOnScreen(array1);
        masOnScreen(array2);
        masOnScreen(array3);
        masOnScreen(array4);
        masOnScreen(array5);

    }

    public static void masOnScreen(int[] mas) {

        for (int i = 0; i < 10; i++) {
            System.out.print(mas[i] + " ");
        }
        System.out.println();
    }
}
