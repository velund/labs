/**
 * Created by Владислав on 22.02.2016.
 * Создать статический метод, который будет иметь два целочисленных параметра
 * a и b, и в качестве своего значения возвращать случайное целое число из отрезка [a;b].
 * C помощью данного метода заполнить массив из 20 целых чисел и вывести его на экран.
 */
package laba4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sa = reader.readLine();
        String sb = reader.readLine();
        int a, b;
        //Исключения
        try {
            a = Integer.parseInt(sa);
            b = Integer.parseInt(sb);
            if (a >= b) {
                System.out.println("Ошибка: задан не верный диапазон");
                return;
            }
        } catch (Exception e) {
            System.out.println("Ошибка: не правильный ввод данных");
            return;
        }
        //Массив из 20 элементов
        System.out.println(Task4.sequenceFibonachi(1));
        int[] mas = new int[20];
        for (int i = 0; i < 20; i++) {
            mas[i] = random_AB(a, b);
            System.out.print(mas[i] + " ");
        }
    }
    //Метод выбора случайного числа из диапозона [a,b]
    public static int random_AB(int a, int b) {
        return (int) (Math.random() * (b - a + 1) + a);
    }

}
