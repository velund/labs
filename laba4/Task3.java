/**
 * Created by Владислав on 27.02.2016.
 * Создать метод, который будет сортировать
 * указанный массив по возрастанию любым известным вам способом.
 */
package laba4;

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 26, 25, 8, 9};
        sortArray(arr);
        System.out.println(Arrays.toString(arr));
    }

    //Метод сортировки пузырьком
    public static void sortArray(int[] arr) {
        int min;
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    min = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = min;
                }

            }

        }
    }
}
